#include <stdint.h>
#include <stdio.h>

#include "tests/tests.h"
#include "tests/test_utils.h"

int main() {
    //тут ничего интересного
    //       _
    //     >(.)__
    //      (___/
    //~~~~~~~~~~~~~~~~~~~~
    for (uint8_t i = 0; i < test_count; i++) {
        print_header(i+1);
        test_list[i]();
    }
    return 0;
}