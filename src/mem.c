#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
  //check region size, calculate real size
  size_t real_size = region_actual_size(size_from_capacity((block_capacity){.bytes = query}).bytes);
  void* pointer = map_pages(addr, real_size, MAP_FIXED_NOREPLACE); // for page to be extensible
  if (pointer == MAP_FAILED) { 
    pointer = map_pages(addr, real_size, 0);  // no add. flags
  }
  if (pointer == MAP_FAILED) { return REGION_INVALID; }
  const struct region reg = (struct region) { .addr = pointer, .extends = addr==pointer, .size = real_size };
  //instantly init block here
  block_init(reg.addr, (block_size) {.bytes = reg.size}, NULL); 
  return reg;
}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */
static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
  if (!block_splittable(block, query)) return false;
  void* new_bl_addr = block->contents + query;
  // if bigger init new block inside one and fix old
  block_init(new_bl_addr, (block_size) {.bytes = block->capacity.bytes - query}, block->next);
  block->capacity.bytes = query;
  block->next = (struct block_header*) new_bl_addr;
  return true;
}

/*  --- Слияние соседних свободных блоков --- */
static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  if (block->next == NULL || !mergeable(block, block->next)) return false;
  block->capacity.bytes += size_from_capacity(block->next->capacity).bytes;
  block->next = block->next->next;
  return true;
}

/*  --- ... ecли размера кучи хватает --- */
struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz ) {
  if (block == NULL)
    return (struct block_search_result) { .block = block, .type = BSR_CORRUPTED };
  //iterate through blocks trying to merge them from stated
  struct block_header* loc_block = block;
  while (loc_block) {
    if (loc_block->is_free) {
      while(try_merge_with_next(loc_block)) {}
      if (block_is_big_enough(sz, loc_block)) {
        return (struct block_search_result) { .block = loc_block, .type = BSR_FOUND_GOOD_BLOCK };
      }
    }
    if (!loc_block->next) break;
    loc_block = loc_block->next;
  }
  //block pointing to last block
  return (struct block_search_result) { .block = loc_block, .type = BSR_REACHED_END_NOT_FOUND };
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  struct block_search_result search = find_good_or_last(block, query);
  if (search.type == BSR_FOUND_GOOD_BLOCK) {
    split_if_too_big(search.block, query);
    search.block->is_free = false;
  }
  return search;
}

static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  if (last == NULL) return NULL;
  const struct region reg = alloc_region(block_after(last), query);
  //cannot use func(ptr is null)
  if (last->is_free && !region_is_invalid(&reg) && reg.extends) {
    last->capacity.bytes += reg.size;
    return last;
  } else {
    //if null nothing changes
    last->next = reg.addr;
    return reg.addr; //block created in alloc
  }
  //returns last free block(old or new)
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
  query = size_max(query, BLOCK_MIN_CAPACITY);
  struct block_search_result att = try_memalloc_existing(query, heap_start);
  if (att.type == BSR_CORRUPTED) return NULL;
  if (att.type == BSR_REACHED_END_NOT_FOUND) {
    att.block = grow_heap(att.block, query);
    //if failed null
    if (!att.block) return NULL;
    //allocated big block, now split it
    att = try_memalloc_existing(query, att.block);
  }
  return att.block;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  while (try_merge_with_next(header));
}
