#include "test_utils.h"

void print_header(int num) {
    fprintf(stdout, "\t--Running test %d--\n", num);
}

void print_message(char* message) {
    fprintf(stdout, "%s", message);
}

void print_error(char* message) {
    fprintf(stderr, "%s", message);
}

void unmap_manually(void* addr, size_t bytes) {
    munmap(addr, size_from_capacity(
        (block_capacity){.bytes = bytes}
        ).bytes);
}

struct block_header* get_block_head_from_ptr(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}
