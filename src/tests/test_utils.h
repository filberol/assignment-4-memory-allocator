#ifndef TEST_UTILS_H
#define TEST_UTILS_H

#include "../mem.h"
#include "../mem_internals.h"
#include <stddef.h>
#include <stdio.h>

void print_header(int num);
void print_message(char* message);
void print_error(char* message);

void unmap_manually(void* addr, size_t bytes);
struct block_header* get_block_head(void* block_addr);

#endif
