#include "tests.h"
#include "test_utils.h"

#define HEAP_SIZE (20 * 4096)
#define SMALL_BLOCK 3000

void test_usual_map() {
    void* heap = heap_init(HEAP_SIZE);
    if (!heap){
        print_error("Error initializing heap.\n");
        return;
    }

    void* emptyBlock = _malloc(SMALL_BLOCK);
    debug_heap(stdout, heap);
    if (!emptyBlock){
        print_error("Memory wasn't allocated.\n");
        return;
    }

    _free(emptyBlock);
    unmap_manually(heap, HEAP_SIZE);
    print_message("Test finished.\n");
}

void unmap_one_of_many() {
    void* heap = heap_init(HEAP_SIZE);

    void* block_1 = _malloc(SMALL_BLOCK);
    void* block_2 = _malloc(SMALL_BLOCK);
    void* block_3 = _malloc(SMALL_BLOCK);

    if (!block_1 || !block_2 || !block_3){
        print_error("Error allocating blocks.\n");
        return;
    }
    _free(block_2);

    if (!block_1 || !block_3){
        print_error("Freeing one block caused many to break.\n");
        return;
    }
    debug_heap(stdout, heap);

    _free(block_1);
    _free(block_3);

    unmap_manually(heap, HEAP_SIZE);
    print_message("Test finished.\n");
}

void unmap_many_blocks() {
    void* heap = heap_init(HEAP_SIZE);

    void* block_1 = _malloc(SMALL_BLOCK);
    void* block_2 = _malloc(SMALL_BLOCK);
    void* block_3 = _malloc(SMALL_BLOCK);
    void* block_4 = _malloc(SMALL_BLOCK);

    _free(block_2);
    _free(block_3);
    debug_heap(stdout, heap);

    if (!block_1 || !block_4){
        print_error("Freeing close standing blocks affected others.\n");
        return;
    }

    print_message("Blocks should merge.\n");
    void* newBlock = _malloc(SMALL_BLOCK * 4);
    debug_heap(stdout, heap);

    if (!block_1 || !block_4){
        print_error("Freeing close standing blocks affected others.\n");
        return;
    }
    _free(block_1);
    _free(block_4);
    _free(newBlock);
    
    unmap_manually(heap, HEAP_SIZE);
    print_message("Test finished.\n");
}

void allocate_bigger_block() {
    void* heap = heap_init(HEAP_SIZE);

    void* block_1 = _malloc(SMALL_BLOCK);
    void* block_2 = _malloc(SMALL_BLOCK);
    void* block_3 = _malloc(SMALL_BLOCK);
    _free(block_2);
    debug_heap(stdout, heap);

    print_message("New block should not take first free block.\n");
    void* big_block = _malloc(SMALL_BLOCK * 3);
    debug_heap(stdout, heap);

    _free(block_1);
    _free(block_3);
    _free(big_block);

    unmap_manually(heap, HEAP_SIZE);
    print_message("Test finished.\n");
}

void extend_heap() {
    void* heap = heap_init(HEAP_SIZE);

    void* big_block = _malloc(HEAP_SIZE - SMALL_BLOCK);
    debug_heap(stdout, heap);

    print_message("New block should map more place.\n");
    void* overdose = _malloc(SMALL_BLOCK * 3);
    debug_heap(stdout, heap);

    _free(big_block);
    _free(overdose);

    unmap_manually(heap, HEAP_SIZE);
    print_message("Test finished.\n");
}
