#ifndef TESTS_H
#define TESTS_H

#include <stdio.h>

#define test_count 5

typedef void (test_function)();

void test_usual_map();
void unmap_one_of_many();
void unmap_many_blocks();
void allocate_bigger_block();
void extend_heap();

static test_function* const test_list[test_count] = {
    test_usual_map,
    unmap_one_of_many,
    unmap_many_blocks,
    allocate_bigger_block,
    extend_heap
};

#endif
